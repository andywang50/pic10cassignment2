#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void calculate();
    void course_changed(int);

private:
    void PIC10BSchemeA();
    void PIC10BSchemeB();
    void PIC10CSchemeA();
    void PIC10CSchemeB();

    void PIC10BSelected();
    void PIC10CSelected();

    void resetAll();

    double PIC10BHWScore();
    double PIC10CHWScore();
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
