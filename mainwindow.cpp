#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::calculate(){
    int course = ui->comboBox->currentIndex();
    bool is_scheme_A = ui->radioButton_A->isChecked();
    if (course == 0){
        if (is_scheme_A) return this->PIC10BSchemeA();
        else return this->PIC10BSchemeB();

    }
    else{
        if (is_scheme_A) return this->PIC10CSchemeA();
        else return this->PIC10CSchemeB();
    }


}

void MainWindow::course_changed(int course){
    this->resetAll();
    if (course == 0) this->PIC10BSelected();
    else this->PIC10CSelected();
}

double MainWindow::PIC10BHWScore(){
    int hw[9];

    hw[0] = 0;
    hw[1] = ui->horizontalSlider_01->value();
    hw[2] = ui->horizontalSlider_02->value();
    hw[3] = ui->horizontalSlider_03->value();
    hw[4] = ui->horizontalSlider_04->value();
    hw[5] = ui->horizontalSlider_05->value();
    hw[6] = ui->horizontalSlider_06->value();
    hw[7] = ui->horizontalSlider_07->value();
    hw[8] = ui->horizontalSlider_08->value();

    int minhw = 101;
    int sumhw = 0;
    for (int i = 1; i < 9; ++i){
        sumhw += hw[i];
        if (hw[i] < minhw) minhw = hw[i];
    }

    sumhw = sumhw - minhw;
    double avghw = 1.0*sumhw/7;
    return avghw;
}

double MainWindow::PIC10CHWScore(){
    return 0.0;
}

void MainWindow::PIC10BSchemeA(){
    double hwscore = this->PIC10BHWScore();
    double mid1score = ui->horizontalSlider_Mid1->value();
    double mid2score = ui->horizontalSlider_Mid2->value();
    double finalscore = ui->horizontalSlider_Final->value();
    QString totalscore = QString::number(0.25*hwscore + 0.2*(mid1score+mid2score)+0.35*finalscore);
    ui->label_score->setText(totalscore);


}

void MainWindow::PIC10BSchemeB(){
    double hwscore = this->PIC10BHWScore();
    double mid1score = ui->horizontalSlider_Mid1->value();
    double mid2score = ui->horizontalSlider_Mid2->value();
    double maxmid = mid1score;
    if (mid2score>maxmid) maxmid = mid2score;
    double finalscore = ui->horizontalSlider_Final->value();
    QString totalscore = QString::number(0.25*hwscore + 0.3*(maxmid)+0.44*finalscore);
    ui->label_score->setText(totalscore);
}

void MainWindow::PIC10CSchemeA(){
    double hw1 = ui->horizontalSlider_01->value();
    double hw2 = ui->horizontalSlider_02->value();
    double final_project = ui->horizontalSlider_FinalProject->value();
    double mid = ui->horizontalSlider_Mid1->value();
    double final = ui->horizontalSlider_Final->value();
    QString totalscore = QString::number(0.075*hw1+0.075*hw2+0.2*mid+0.3*final+0.35*final_project);
    ui->label_score->setText(totalscore);
}

void MainWindow::PIC10CSchemeB(){
    double hw1 = ui->horizontalSlider_01->value();
    double hw2 = ui->horizontalSlider_02->value();
    double final_project = ui->horizontalSlider_FinalProject->value();
    double final = ui->horizontalSlider_Final->value();
    QString totalscore = QString::number(0.075*hw1+0.075*hw2+0.5*final+0.35*final_project);
    ui->label_score->setText(totalscore);
}

void MainWindow::PIC10BSelected(){
    ui->spinBox_FinalProject->setEnabled(false);
    ui->horizontalSlider_FinalProject->setEnabled(false);
    ui->spinBox_Mid2->setEnabled(true);
    ui->horizontalSlider_Mid2->setEnabled(true);
    ui->spinBox_03->setEnabled(true);
    ui->horizontalSlider_03->setEnabled(true);
    ui->spinBox_04->setEnabled(true);
    ui->horizontalSlider_04->setEnabled(true);
    ui->spinBox_05->setEnabled(true);
    ui->horizontalSlider_05->setEnabled(true);
    ui->spinBox_06->setEnabled(true);
    ui->horizontalSlider_06->setEnabled(true);
    ui->spinBox_07->setEnabled(true);
    ui->horizontalSlider_07->setEnabled(true);
    ui->spinBox_08->setEnabled(true);
    ui->horizontalSlider_08->setEnabled(true);
}

void MainWindow::PIC10CSelected(){
    ui->spinBox_FinalProject->setEnabled(true);
    ui->horizontalSlider_FinalProject->setEnabled(true);
    ui->spinBox_Mid2->setEnabled(false);
    ui->horizontalSlider_Mid2->setEnabled(false);
    ui->spinBox_03->setEnabled(false);
    ui->horizontalSlider_03->setEnabled(false);
    ui->spinBox_04->setEnabled(false);
    ui->horizontalSlider_04->setEnabled(false);
    ui->spinBox_05->setEnabled(false);
    ui->horizontalSlider_05->setEnabled(false);
    ui->spinBox_06->setEnabled(false);
    ui->horizontalSlider_06->setEnabled(false);
    ui->spinBox_07->setEnabled(false);
    ui->horizontalSlider_07->setEnabled(false);
    ui->spinBox_08->setEnabled(false);
    ui->horizontalSlider_08->setEnabled(false);

}

void MainWindow::resetAll(){
    ui->spinBox_01->setValue(0);
    ui->spinBox_02->setValue(0);
    ui->spinBox_03->setValue(0);
    ui->spinBox_04->setValue(0);
    ui->spinBox_05->setValue(0);
    ui->spinBox_06->setValue(0);
    ui->spinBox_07->setValue(0);
    ui->spinBox_08->setValue(0);
    ui->spinBox_FinalProject->setValue(0);
    ui->spinBox_Mid1->setValue(0);
    ui->spinBox_Mid2->setValue(0);
    ui->spinBox_Final->setValue(0);
    ui->radioButton_A->setChecked(true);


}
